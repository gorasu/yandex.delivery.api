<?php

namespace GoraSu\Components\YandexDeliveryApi\Tests\Unit\Request;

use GoraSu\Components\YandexDeliveryApi\Request\RequestInterface;
use PHPUnit\Framework\TestCase;

class RequestInterfaceTest extends TestCase
{

    /**
     * Тест проверяет что в интерфейсе верно объявлены константы
     */
    function testConstant (){



    /*<case>*/
        $this->assertEquals("POST", RequestInterface::POST);
        $this->assertEquals("GET", RequestInterface::GET);
        $this->assertEquals("PUT", RequestInterface::PUT);
        $this->assertEquals("PATCH", RequestInterface::PATCH);
        $this->assertEquals("DELETE", RequestInterface::DELETE);
    /*</case>*/



    }

}
