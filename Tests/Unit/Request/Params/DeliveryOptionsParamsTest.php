<?php


namespace GoraSu\Components\YandexDeliveryApi\Tests\Unit\Request\Params;

use Doctrine\Common\Annotations\AnnotationRegistry;
use GoraSu\Components\YandexDeliveryApi\Request\Params\DeliveryOptionsParams;
use JMS\Serializer\Naming\IdenticalPropertyNamingStrategy;
use JMS\Serializer\Naming\SerializedNameAnnotationStrategy;
use JMS\Serializer\SerializerBuilder;
use PHPUnit\Framework\TestCase;
use JMS\Serializer\Annotation\Type;


class DeliveryOptionsParamsTest extends TestCase
{


    /**
     * Тест проверяет что парамтетры коррентно конвертируются в JSON строку
     */
    function testToJson (){

    /*<precondition>*/
        $deliveryOptionParams = new DeliveryOptionsParams();
        $deliveryOptionParams->setSenderId(500);
        $deliveryOptionParams->to()->setLocation("Россия Москва Троицк");
        $deliveryOptionParams->setDeliveryType('PICKUP');
        $deliveryOptionParams->dimensions()->setHeight(30)->setLength(10)->setWidth(20)->setWeight(5.25);
        $deliveryOptionParams->cost()->setItemsSum(125)->setFullyPrepaid(true)->setAssessedValue(123);
    /*</precondition>*/

    /*<case>*/
        AnnotationRegistry::registerLoader('class_exists');
        $serializer = SerializerBuilder::create()
            ->setPropertyNamingStrategy(
                new SerializedNameAnnotationStrategy(
                    new IdenticalPropertyNamingStrategy()
                )
            )
            ->build();
        $json = $serializer->serialize($deliveryOptionParams, "json");

    /*</case>*/

    /*<postcondition>*/
        $this->assertJsonStringEqualsJsonString('{
    "senderId": 500,
    "to": {
        "location":"Россия Москва Троицк"

    },
    "deliveryType":"PICKUP",
    "dimensions": {
        "length": 10,
        "width": 20,
        "height": 30,
        "weight": 5.25
    },
     "cost": {
        "assessedValue": 123,
        "itemsSum": 125,
        "fullyPrepaid": true
    }

}', $json);
    /*</postcondition>*/

    }



}
