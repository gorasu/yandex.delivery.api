<?php

namespace GoraSu\Components\YandexDeliveryApi\Tests\Unit\Request\Params;

use GoraSu\Components\YandexDeliveryApi\Request\Params\OrderDraftParams;
use GoraSu\Components\YandexDeliveryApi\Request\Type\OrderDraft\Address;
use GoraSu\Components\YandexDeliveryApi\Request\Type\OrderDraft\Contact;
use GoraSu\Components\YandexDeliveryApi\Request\Type\OrderDraft\Cost;
use GoraSu\Components\YandexDeliveryApi\Request\Type\OrderDraft\DeliveryOption;
use GoraSu\Components\YandexDeliveryApi\Request\Type\OrderDraft\Dimensions;
use GoraSu\Components\YandexDeliveryApi\Request\Type\OrderDraft\Item;
use GoraSu\Components\YandexDeliveryApi\Request\Type\OrderDraft\Recipient;
use GoraSu\Components\YandexDeliveryApi\Request\Type\OrderDraft\Services;
use GoraSu\Components\YandexDeliveryApi\Request\Type\OrderDraft\Shipment;
use GoraSu\Components\YandexDeliveryApi\Tests\UnitTestCase;
use GoraSu\Components\YandexDeliveryApi\Type\Measure\Height;
use GoraSu\Components\YandexDeliveryApi\Type\Measure\Length;
use GoraSu\Components\YandexDeliveryApi\Type\Measure\Weight;
use GoraSu\Components\YandexDeliveryApi\Type\Measure\Width;

class OrderDraftParamsTest extends UnitTestCase
{

    /**
     * Тест проверяет что при добавлении товара добавляются грузоместа на каждую позицию товара
     */
    function testAddProductItem__place_for_one_item()
    {

        /*<precondition>*/
        $create = new OrderDraftParams('', '');
        /*</precondition>*/

        /*<case>*/
        $item = new Item('Товар 1', 200, 3, Item::VAT_20);
        $item->setDimensions(Dimensions::create(
            Width::createByCm(20),
            Height::createByCm(20),
            Length::createByCm(20),
            Weight::createByGram(2000)));

        $create->addProductItem($item);
        /*</case>*/

        /*<postcondition>*/
        $this->assertCount(3, $create->getPlaces());
        foreach ($create->getPlaces() as $place) {
            $this->assertEquals($item->getDimensions(), $place->getDimensions());
        }
        /*</postcondition>*/

    }

    /**
     * Тест проверяет что при добавлении разных товаров добавляются грузоместа на каждую позицию товара
     */
    function testAddProductItem__place_for_other_item()
    {

        /*<precondition>*/
        $create = new OrderDraftParams('', '');
        /*</precondition>*/

        /*<case>*/
        $item = new Item('Товар 1', 200, 1, Item::VAT_20);
        $item->setDimensions(Dimensions::create(
            Width::createByCm(20),
            Height::createByCm(20),
            Length::createByCm(20),
            Weight::createByGram(2000)));

        $item2 = new Item('Товар 2', 200, 1, Item::VAT_20);
        $item2->setDimensions(Dimensions::create(
            Width::createByCm(20),
            Height::createByCm(100),
            Length::createByCm(20),
            Weight::createByGram(2000)));

        $create->addProductItem($item);
        $create->addProductItem($item2);
        /*</case>*/

        /*<postcondition>*/
        $this->assertCount(2, $create->getPlaces());
        $this->assertEquals($item->getDimensions(), $create->getPlaces()[0]->getDimensions());
        $this->assertEquals($item2->getDimensions(), $create->getPlaces()[1]->getDimensions());

        /*</postcondition>*/

    }

    /**
     * Тест проверяет что данные запроса конвертируются в JSON
     */
    function testToJson()
    {

        /*<precondition>*/
        //@todo data for prepare test
        /*</precondition>*/

        /*<case>*/
        $orderDraftParams = new OrderDraftParams(123, OrderDraftParams::DELIVERY_TYPE_COURIER);
        $orderDraftParams->setExternalId('23442');
        $orderDraftParams->setComment('Комментарий');
        $recipient = new Recipient();

        $orderDraftParams->setRecipient((new Recipient())
            ->setFirstName('Иван')
            ->setMiddleName('Сергеевич')
            ->setLastName('Иванов')
            ->setEmail('ivan@ivan.ru')
            ->setAddress((new Address())
                ->setGeoId(324)
                ->setCountry('Россия')
                ->setRegion('Московская Область')
                ->setSubRegion('Центр')
                ->setLocality("Москва")
                ->setStreet('Школьная')
                ->setHouse('3')
                ->setHousing('5')
                ->setBuilding('8')
                ->setApartment('78')
                ->setPostalCode('180180')
                ->setPostCode('180181')


            )
            ->setFullAddress('Россия, Московская Область, Москва, Гурьева 3 56')
            ->setPickupPointId('32424')
        );
        $orderDraftParams->setCost((new Cost())->setManualDeliveryForCustomer(20.556)->setAssessedValue(21.4)->setFullyPrepaid(true)->setPaymentMethod('CASH'));
        $orderDraftParams->setContacts([

            (new Contact())
                ->setType('RECIPIENT')
                ->setPhone('89080798979')
                ->setAdditional('89165440000')
                ->setFirstName('Петр')
                ->setMiddleName('Александрович')
                ->setLastName('Плюшкин')


        ]);
        $orderDraftParams->setDeliveryOption((new DeliveryOption())
            ->setTariffId(644)
            ->setDelivery(655.446)
            ->setDeliveryForCustomer(656.427)
            ->setPartnerId(546)
            ->setDeliveryIntervalId(131)
            ->setCalculatedDeliveryDateMin(new \DateTime('2021-05-01'))
            ->setCalculatedDeliveryDateMax(new \DateTime('2021-05-11'))
            ->setServices([
                (new Services())->setCode('DELIVERY')
                    ->setCost(23.426)
                    ->setCustomerPay(true)
            ])
        );

        $orderDraftParams->setShipment((new Shipment('WITHDRAW'))
            ->setDate(new \DateTime('2021-04-22'))
            ->setWarehouseFrom(123)
            ->setWarehouseTo(432)
            ->setPartnerTo(343)
        );

        $orderDraftParams->addProductItem((new Item('Товар 1', '200', '2', Item::VAT_20))
            ->setExternalId(3242)
            ->setAssessedValue(321)
            ->setDimensions((
            Dimensions::create(Width::createByCm(32), Height::createByCm(12), Length::createByCm(12), Weight::createByKilogram(3)))));


        $json = $this->serializer->serialize($orderDraftParams, "json");
        /*</case>*/

        /*<postcondition>*/
        $this->assertJsonStringEqualsJsonString($this->getExpectedJson(), $json);
        /*</postcondition>*/

    }


    private function getExpectedJson()
    {
        return '{
  "senderId": 123,
  "externalId": "23442",
  "comment": "Комментарий",
  "deliveryType": "COURIER",
  "recipient":
  {
    "firstName": "Иван",
    "middleName": "Сергеевич",
    "lastName": "Иванов",
    "email": "ivan@ivan.ru",
    "address":
    {
      "geoId": 324,
      "country": "Россия",
      "region": "Московская Область",
      "subRegion": "Центр",
      "locality": "Москва",
      "street": "Школьная",
      "house": "3",
      "housing": "5",
      "building": "8",
      "apartment": "78",
      "postalCode": "180180",
      "postCode": "180181"
    },
    "fullAddress": "Россия, Московская Область, Москва, Гурьева 3 56",
    "pickupPointId": 32424
  },
  "cost":
  {
    "manualDeliveryForCustomer": 20.56,
    "paymentMethod": "CASH",
    "assessedValue": 21.4,
    "fullyPrepaid": true
  },
  "contacts":
  [
    {
      "type": "RECIPIENT",
      "phone": "89080798979",
      "additional": "89165440000",
      "firstName": "Петр",
      "middleName": "Александрович",
      "lastName": "Плюшкин"
    }
  ],
  "deliveryOption":
  {
    "tariffId": 644,
    "delivery": 655.45,
    "deliveryForCustomer": 656.43,
    "partnerId": 546,
    "deliveryIntervalId": 131,
    "calculatedDeliveryDateMin": "2021-05-01",
    "calculatedDeliveryDateMax": "2021-05-11",
    "services":
    [
      {
        "code": "DELIVERY",
        "cost": 23.43,
        "customerPay": true
      }
    ]
  },
  "shipment":
  {
    "type": "WITHDRAW",
    "date": "2021-04-22",
    "warehouseFrom": 123,
    "warehouseTo": 432,
    "partnerTo": 343
  },
  "places":
  [
    {
      "dimensions":
      {
             "length" : "12",
                "width" : "32",
                "height" : "12",
                "weight" : "3"
      }
     
    },{
      "dimensions":
      {
             "length" : "12",
                "width" : "32",
                "height" : "12",
                "weight" : "3"
      }
     
    }
   
    
  ],
   "items":
      [
        {
          "externalId": "3242",
          "name": "Товар 1",
          "count": 2,
          "price": 200,
          "assessedValue": 321,
          "tax": "VAT_20",
          "dimensions":
          {
            "length": 12,
            "width": 32,
            "height": 12,
            "weight": 3
          }
        }
        
      ]
}';


    }

}
