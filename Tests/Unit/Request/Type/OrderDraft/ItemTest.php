<?php

namespace GoraSu\Components\YandexDeliveryApi\Tests\Unit\Request\Type\OrderDraft;

use GoraSu\Components\YandexDeliveryApi\Request\Type\OrderDraft\Item;
use PHPUnit\Framework\TestCase;

class ItemTest extends TestCase
{

    /**
     * Тест проверяет что можно установить корректную ставку НДС
     * @dataProvider providerSetTax
     */
    function testSetTax($vat){


    /*<case>*/
        $item = new Item('Item',100, 1, $vat);
    /*</case>*/

    /*<postcondition>*/
        $this->assertEquals($vat, $item->getTax());
    /*</postcondition>*/

    }

    /**
     * Тест проверяет что при установке не верной ставки выведется исключение
     */
    function testSetTax__fail (){


        /*<postcondition>*/
        $this->expectException(\DomainException::class);
        $possibleTax = [Item::NO_VAT, Item::VAT_0, Item::VAT_10, Item::VAT_20];
        $this->expectExceptionMessage('Tax mast be one of '.implode(', ',$possibleTax));
        /*</postcondition>*/

        /*<case>*/
        $item = new Item('Item',100, 1, 'VAT_100');
        /*</case>*/



    }

    /**
     * Тест проверяет что корректно округляется стоимость товара
     */
    function testPriceRound (){

        /*<precondition>*/
        $item = new Item('',100.555,2, Item::NO_VAT);
        $item->setAssessedValue(101.555);
        /*</precondition>*/

        /*<case>*/
        //@todo test logic
        /*</case>*/

        /*<postcondition>*/
        $this->assertEquals(100.56, $item->getPrice());
        $this->assertEquals(101.56, $item->getAssessedValue());
        /*</postcondition>*/

    }

    public function providerSetTax()
    {
        return [

            'Item::VAT_20'=>[
                Item::VAT_20
            ],
            'Item::VAT_10'=>[
                Item::VAT_10
            ],
            'Item::VAT_0'=>[
                Item::VAT_0
            ],
            'Item::NO_VAT'=>[
                Item::NO_VAT
            ],

        ];
    }

}
