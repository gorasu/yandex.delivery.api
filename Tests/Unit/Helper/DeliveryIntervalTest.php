<?php

namespace GoraSu\Components\YandexDeliveryApi\Tests\Unit\Helper;

use GoraSu\Components\YandexDeliveryApi\Helper\DeliveryInterval;
use PHPUnit\Framework\TestCase;

class DeliveryIntervalTest extends TestCase
{


    /**
     * Тест который проверяет что высчитывается корректный интервал в днях
     * @dataProvider providerGetDayInterval
     */
    public function testGetDayInterval($start, $end, $expected)
    {

        $date = new \DateTime();
        $dateStart = clone $date;
        $dateStart->add(new \DateInterval('P'.$start.'D'));
        $dateEnd = clone $date;
        $dateEnd->add(new \DateInterval('P'.$end.'D'));


        $deliveryInterval = new DeliveryInterval($dateStart, $dateEnd);
        $this->assertEquals($expected,$deliveryInterval->getDayInterval());
        $this->assertEquals($start,$deliveryInterval->getDayMin());
        $this->assertEquals($end,$deliveryInterval->getDayMax());


    }

    public function providerGetDayInterval()
    {
        return [

            'Разный интервал'=> ['start'=>1, 'end'=>5, '1-5 day']
            , 'Конец меньше начала  '=> ['start'=>5, 'end'=>1, '5-1 day']
            ,'Одинаковый интервал'=> ['start'=>1, 'end'=>1, '1 day']
            ,'Нулевой интервал начала'=> ['start'=>0, 'end'=>1, '1 day']
            ,'Нулевой интервал начала и конец больше 1'=> ['start'=>0, 'end'=>4, '1-4 day']
            ,'Нулевой интервал конца'=> ['start'=>1, 'end'=>0, '1 day']
            ,'Нулевой интервал '=> ['start'=>0, 'end'=>0, '1 day']

        ];
    }
}
