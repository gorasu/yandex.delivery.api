<?php

namespace GoraSu\Components\YandexDeliveryApi\Tests\Unit\Type\Measure;

use GoraSu\Components\YandexDeliveryApi\Type\Measure\Weight;
use PHPUnit\Framework\TestCase;

class WeightTest extends TestCase
{

    /**
     * Тест проверяет что создается весь в килограммах
     */
    function testCreateByKilogram (){


    /*<case>*/

        $weight = Weight::createByKilogram(1);

    /*</case>*/

    /*<postcondition>*/
        $this->assertEquals(1, $weight->getKilogram());
    /*</postcondition>*/

    }

    /**
     * Тест проверяет что создается вес из граммов
     */
    function testCreateByGram (){


    /*<case>*/
        $weight = Weight::createByGram(2300);
    /*</case>*/

    /*<postcondition>*/
        $this->assertEquals(2.3, $weight->getKilogram());
    /*</postcondition>*/

    }
}
