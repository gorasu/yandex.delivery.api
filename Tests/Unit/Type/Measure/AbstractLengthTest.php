<?php

namespace GoraSu\Components\YandexDeliveryApi\Tests\Unit\Type\Measure;

use GoraSu\Components\YandexDeliveryApi\Type\Measure\AbstractLength;
use GoraSu\Components\YandexDeliveryApi\Type\Measure\Height;
use GoraSu\Components\YandexDeliveryApi\Type\Measure\Length;
use GoraSu\Components\YandexDeliveryApi\Type\Measure\Width;
use PHPUnit\Framework\TestCase;

class AbstractLengthTest extends TestCase
{



    /**
     * Тест проверяет что создается объект длины в сантиметрах
     * @dataProvider providerCreate
     */
    function testCreateByCm ($class){

    /*<precondition>*/

    /*</precondition>*/

    /*<case>*/
        /** @var AbstractLength $result */
        $result = $class::createByCm(100);
    /*</case>*/

    /*<postcondition>*/
        $this->assertEquals(100, $result->getInCm());
        $this->assertInstanceOf($class, $result);
    /*</postcondition>*/

    }


    /**
     * Тест проверяет что создается объект длины в сантиметрах
     * @dataProvider providerCreate
     */
    function testCreateByMeter ($class){

        /*<precondition>*/

        /*</precondition>*/

        /*<case>*/
        /** @var AbstractLength $result */
        $result = $class::createByMeter(10);
        /*</case>*/

        /*<postcondition>*/
        $this->assertEquals(1000, $result->getInCm());
        $this->assertInstanceOf($class, $result);
        /*</postcondition>*/

    }

    /**
     * Тест проверяет что создается объект длины в сантиметрах
     * @dataProvider providerCreate
     */
    function testCreateByMilliMeter ($class){

        /*<precondition>*/

        /*</precondition>*/

        /*<case>*/
        /** @var AbstractLength $result */
        $result = $class::createByMilliMeter(200);
        /*</case>*/

        /*<postcondition>*/
        $this->assertEquals(20, $result->getInCm());
        $this->assertInstanceOf($class, $result);
        /*</postcondition>*/

    }

    public function providerCreate()
    {
        return [

            'Height' => [
                Height::class
            ],

            'Width' => [
                Width::class
            ],

            'Length' => [
                Length::class
            ]

        ];
    }


}
