<?php


namespace GoraSu\Components\YandexDeliveryApi\Tests\Unit;


use GoraSu\Components\YandexDeliveryApi\GuzzleBuilder;
use PHPUnit\Framework\TestCase;

class GuzzleBuilderTest extends TestCase
{

    /**
     * Тест проверяет что билдер создает корректный клиент Guzzle
     */
    function testBuild (){

    /*<precondition>*/
        $authKey = "eijdweiojd23ijoej23oiej3io2ejioj3io2j";
        $baseUrl = "https://example.com";
        $builder = new GuzzleBuilder();
    /*</precondition>*/

    /*<case>*/
        $client = $builder->build('https://example.com', $authKey);

    /*</case>*/

    /*<postcondition>*/
        $config = $client->getConfig();
        $this->assertEquals("OAuth $authKey", $config['headers']['Authorization']);
        $this->assertEquals("GoraSuYandexAPI/1.0", $config['headers']['User-Agent']);
        $this->assertEquals($baseUrl, $config['base_uri']->__toString());
    /*</postcondition>*/

    }

}