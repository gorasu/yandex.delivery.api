<?php


namespace GoraSu\Components\YandexDeliveryApi\Tests\Integration;


use GoraSu\Components\YandexDeliveryApi\Client;
use GoraSu\Components\YandexDeliveryApi\GuzzleBuilder;
use GoraSu\Components\YandexDeliveryApi\Request\Location;
use GoraSu\Components\YandexDeliveryApi\Request\RequestInterface;
use GoraSu\Components\YandexDeliveryApi\Tests\IntegrationTestCase;
use JMS\Serializer\SerializerInterface;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;

class ClientTest extends IntegrationTestCase
{


    /**
     * Тест который проверяет что клиент строит запрос на базе интерфейса
     */
    function testRequest(){

        /** @var SerializerInterface $serializer */
        $serializer = $this->getMockBuilder(SerializerInterface::class)->getMock();
        $client = new Client($this->getApiKey(), new GuzzleBuilder(), $serializer);
        /** @var RequestInterface| PHPUnit_Framework_MockObject_MockObject $request */
        $request = $this->getMockBuilder(RequestInterface::class)->getMock();
        $request->method("getHttpMethod")->willReturn('GET');
        $request->method("getApiMethod")->willReturn("location");
        $request->method("getRequestParams")->willReturn(['term'=>'Россия, Москва, Троицк']);
        $request->method("getResponseClassName")->willReturn(null);

        $response = $client->request($request);
        $this->assertEquals(20674,$response[0]['geoId']);

    }

}