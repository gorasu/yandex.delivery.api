<?php

namespace GoraSu\Components\YandexDeliveryApi\Tests\Integration\Request;

use GoraSu\Components\YandexDeliveryApi\ClientBuilder;
use GoraSu\Components\YandexDeliveryApi\Request\Location;
use GoraSu\Components\YandexDeliveryApi\Request\Params\PickupPointsParams;
use GoraSu\Components\YandexDeliveryApi\Request\PickupPoints;
use GoraSu\Components\YandexDeliveryApi\Tests\IntegrationTestCase;
use PHPUnit\Framework\TestCase;

class PickupPointsTest extends IntegrationTestCase
{

    /**
     * Тест проверяет получение локации
     */
    function testPickupPoints (){

    /*<precondition>*/
        $client = (new ClientBuilder($this->getApiKey()))->build();
    /*</precondition>*/

    /*<case>*/
        $params = new PickupPointsParams();
        $params->setLocationId(213);
        $request =new PickupPoints($params);
        $response = $client->request($request);
    /*</case>*/

    /*<postcondition>*/
        $this->assertGreaterThan(1,$response);
    /*</postcondition>*/

    }
}
