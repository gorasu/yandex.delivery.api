<?php

namespace GoraSu\Components\YandexDeliveryApi\Tests\Integration\Request;

use GoraSu\Components\YandexDeliveryApi\ClientBuilder;
use GoraSu\Components\YandexDeliveryApi\Request\Location;
use GoraSu\Components\YandexDeliveryApi\Tests\IntegrationTestCase;
use PHPUnit\Framework\TestCase;

class LocationTest extends IntegrationTestCase
{

    /**
     * Тест проверяет получение локации
     */
    function testLocation (){

    /*<precondition>*/
        $client = (new ClientBuilder($this->getApiKey()))->build();
    /*</precondition>*/

    /*<case>*/
        $location = new Location('Москва');
        $response = $client->request($location);
    /*</case>*/

    /*<postcondition>*/
        $this->assertEquals(213,$response[0]['geoId']);
    /*</postcondition>*/

    }
}
