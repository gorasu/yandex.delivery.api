<?php


namespace GoraSu\Components\YandexDeliveryApi\Tests\Integration\Request;

use GoraSu\Components\YandexDeliveryApi\ClientBuilder;
use GoraSu\Components\YandexDeliveryApi\Request\CreateOrderDraft;
use GoraSu\Components\YandexDeliveryApi\Request\Params\OrderDraftParams;
use GoraSu\Components\YandexDeliveryApi\Request\Type\OrderDraft\Address;
use GoraSu\Components\YandexDeliveryApi\Request\Type\OrderDraft\Contact;
use GoraSu\Components\YandexDeliveryApi\Request\Type\OrderDraft\DeliveryOption;
use GoraSu\Components\YandexDeliveryApi\Request\Type\OrderDraft\Dimensions;
use GoraSu\Components\YandexDeliveryApi\Request\Type\OrderDraft\Item;
use GoraSu\Components\YandexDeliveryApi\Request\Type\OrderDraft\Place;
use GoraSu\Components\YandexDeliveryApi\Request\Type\OrderDraft\Recipient;
use GoraSu\Components\YandexDeliveryApi\Response\OrderDraftResponse;
use GoraSu\Components\YandexDeliveryApi\Tests\IntegrationTestCase;
use GoraSu\Components\YandexDeliveryApi\Type\Measure\Height;
use GoraSu\Components\YandexDeliveryApi\Type\Measure\Length;
use GoraSu\Components\YandexDeliveryApi\Type\Measure\Weight;
use GoraSu\Components\YandexDeliveryApi\Type\Measure\Width;

class CreateOrderDraftTest extends IntegrationTestCase
{


    /**
     * @var Recipient
     */
    private $recipient;
    /**
     * @var Contact
     */
    private $contact;
    /**
     * @var DeliveryOption
     */
    private $deliveryOption;

    /**
     * @var Item
     */
    private $item;

    function setUp()
    {
        parent::setUp();

        $this->recipient = (new Recipient())
            ->setLastName('Иванов')
            ->setFirstName('Иван')
            ->setEmail('ivan@ivan.ru')
            ->setFullAddress('Россия, Московская Область, Москва, Школьная 4 67');

        $this->contact = (new Contact())
            ->setFirstName('Алексндр')
            ->setLastName('Александров')
            ->setMiddleName('')
            ->setPhone('89260000000')
            ->setType('RECIPIENT');
        $this->deliveryOption =  (new DeliveryOption())
            ->setDelivery(200)
            ->setTariffId(100000)
            ->setPartnerId(133)
            ->setDeliveryForCustomer(250)
            ->setCalculatedDeliveryDateMin(new \DateTime())
            ->setCalculatedDeliveryDateMax((new \DateTime())->modify('+1 day'));

        $this->item = (new Item('Товар 1', 200, 3, Item::VAT_20))->setDimensions( Dimensions::create(
            Width::createByCm(20),
            Height::createByCm(20),
            Length::createByCm(20),
            Weight::createByGram(2000)
        ));

    }


    /**
     * Тест который проверяет создание черновика заказа с полным адресом
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    function testRequest__full_address(){


        $client = (new ClientBuilder($this->getApiKey()))->build();


        $createOrderDraft = new OrderDraftParams($this->getSenderId(), 'COURIER');
        //$createOrderDraft->addPlace($this->place);
        $createOrderDraft->setRecipient($this->recipient );
        $createOrderDraft->addContact($this->contact);
        $createOrderDraft->setDeliveryOption($this->deliveryOption );
        $createOrderDraft->addProductItem($this->item);


            /** @var OrderDraftResponse $order */
            $order = $client->request(new CreateOrderDraft($createOrderDraft));
            $this->assertInstanceOf(OrderDraftResponse::class, $order);
            $this->assertGreaterThan(0, $order->getOrderId());


    }

    /**
     * Тест который проверяет создание черновика с переданным адресом не в одну строку
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    function testRequest__address(){


        $client = (new ClientBuilder($this->getApiKey()))->build();


        $createOrderDraft = new OrderDraftParams($this->getSenderId(), 'COURIER');
        $createOrderDraft->addProductItem($this->item);
        $createOrderDraft->setRecipient($this->recipient->setFullAddress(null)->setAddress(
            (new Address())
                ->setLocality('Москва')
                ->setCountry('Россия')
                ->setRegion('Московская область')
                ->setPostCode('180180')
                ->setStreet('Школьная')
            ->setBuilding(12)
            ->setHouse(3)
            ->setApartment(23)


        ) );
        $createOrderDraft->addContact($this->contact);
        $createOrderDraft->setDeliveryOption($this->deliveryOption );


        /** @var OrderDraftResponse $order */
        $order = $client->request(new CreateOrderDraft($createOrderDraft));
        $this->assertInstanceOf(OrderDraftResponse::class, $order);
        $this->assertGreaterThan(0, $order->getOrderId());


    }

}