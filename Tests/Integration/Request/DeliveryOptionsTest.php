<?php


namespace GoraSu\Components\YandexDeliveryApi\Tests\Integration\Request;

use GoraSu\Components\YandexDeliveryApi\ClientBuilder;
use GoraSu\Components\YandexDeliveryApi\Request\DeliveryOptions;
use GoraSu\Components\YandexDeliveryApi\Request\Params\DeliveryOptionsParams;
use GoraSu\Components\YandexDeliveryApi\Request\Type\DeliveryOptions\Dimensions;
use GoraSu\Components\YandexDeliveryApi\Request\Type\DeliveryOptions\Place;
use GoraSu\Components\YandexDeliveryApi\Response\DeliveryOption;
use GoraSu\Components\YandexDeliveryApi\Response\Type\DeliveryOption\Service;
use GoraSu\Components\YandexDeliveryApi\Response\Type\DeliveryOption\Shipment;
use GoraSu\Components\YandexDeliveryApi\Tests\IntegrationTestCase;

class DeliveryOptionsTest extends IntegrationTestCase
{

    function testRequest(){


        $client = (new ClientBuilder($this->getApiKey()))->build();


        $deliveryParams = new DeliveryOptionsParams();
        $deliveryParams->to()->setLocation("Россия Москва Троицк");
        $deliveryParams->setSenderId($this->getSenderId());
        $deliveryParams->cost()->setAssessedValue(123)->setItemsSum(123)->setFullyPrepaid(false);
        $deliveryParams->dimensions()->setWidth(10)->setWeight(12)->setHeight(10)->setLength(10);
        $deliveryParams->addPlaces((new Place(
            (new Dimensions())
                ->setWidth(10)
                ->setWeight(12)
                ->setHeight(10)
                ->setLength(10)
        )));

        /** @var DeliveryOption[] $deliveryOptions */
        $deliveryOptions = $client->request(new DeliveryOptions($deliveryParams));

        foreach ($deliveryOptions as $deliveryOption){
            $this->assertInternalType("int", $deliveryOption->getTariffId());
            $this->assertInternalType("float", $deliveryOption->getCost()->getDelivery());
            $this->assertInternalType("float", $deliveryOption->getCost()->getDeliveryForCustomer());
            $this->assertInternalType("float", $deliveryOption->getCost()->getDeliveryForSender());
            $this->assertInternalType("array", $deliveryOption->getTags());
            $this->assertInternalType("string", $deliveryOption->getDelivery()->getType());
            $this->assertInstanceOf(\DateTime::class, $deliveryOption->getDelivery()->getCalculatedDeliveryDateMin());
            $this->assertInstanceOf(\DateTime::class, $deliveryOption->getDelivery()->getCalculatedDeliveryDateMax());
            $this->assertInstanceOf(Service::class, $deliveryOption->getServices()[0]);
            $this->assertInstanceOf(Shipment::class, $deliveryOption->getShipments()[0]);
            $this->assertInstanceOf(\DateTime::class, $deliveryOption->getShipments()[0]->getDate());
            $this->assertNotNull($deliveryOption->getServices()[0]->getName());
        }

    }

}