<?php


namespace GoraSu\Components\YandexDeliveryApi\Tests;


use Doctrine\Common\Annotations\AnnotationRegistry;
use JMS\Serializer\Naming\IdenticalPropertyNamingStrategy;
use JMS\Serializer\Naming\SerializedNameAnnotationStrategy;
use JMS\Serializer\Serializer;
use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializerInterface;
use PHPUnit\Framework\TestCase;

/**
 * Кейс для интеграционных тестов
 * Class ConfigIntegration
 * @package GoraSu\Components\YandexDeliveryApi\Tests
 */
class UnitTestCase extends TestCase
{

    protected $serializer;
    protected $serializerMock;

    function setUp()
    {
        parent::setUp(); // TODO: Change the autogenerated stub

        AnnotationRegistry::registerLoader('class_exists');
        $this->serializer = SerializerBuilder::create()
            ->setPropertyNamingStrategy(
                new SerializedNameAnnotationStrategy(
                    new IdenticalPropertyNamingStrategy()
                )
            )
            ->build();

        $this->serializerMock = $this->getMockBuilder(SerializerInterface::class)->getMock();
    }


}