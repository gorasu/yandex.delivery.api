
Пример работы:

```php

 $deliveryOptionParams = new DeliveryOptionsParams();
        $deliveryOptionParams->setSenderId(500);
        $deliveryOptionParams->to()->setLocation("Россия Москва Троицк");
        $deliveryOptionParams->setDeliveryType('PICKUP');
        $deliveryOptionParams->dimensions()->setHeight(30)->setLength(10)->setWidth(20)->setWeight(5.25);
        $deliveryOptionParams->cost()->setItemsSum(125)->setFullyPrepaid(true)->setAssessedValue(123);



```