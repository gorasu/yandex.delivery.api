## Описание 

В окружении нет сервера только php-cli, так как внутри контейнера нет процессов он умирает сразу после запуска.
 
 К примеру: запустили тесты или скрипт, контейнер поднялся, отработал, вывел результат работы скрипта и упал. 



## Если не работает xDebug из "коробки". 

То нужно добавить alias для локальной сети, и прописать его в xDebug config 

*{IP_ALIAS} -нужно заменить на любой ip, к примеру 10.254.254.254

**MacOs**  
`sudo ifconfig lo0 alias {IP_ALIAS}`  

**Linux**   
`sudo ifconfig lo:143  {IP_ALIAS}`


В файле docker/.env, отредактировать переменную XDEBUG_CONFIG, добавив в параметр remote_host созданный IP адрес.     

`XDEBUG_CONFIG=idekey=PHPSTORM remote_host={IP_ALIAS} profiler_enable=0 remote_enable=1`


