<?php


namespace GoraSu\Components\YandexDeliveryApi\Helper;


/**
 * класс кторый помогает расчитать интервал доставки
 * Class DeliveryInterval
 * @package GoraSu\Components\YandexDeliveryApi\Helper
 */
class DeliveryInterval

{

    /**
     * @var \DateTime
     */
    private $dateStart;
    /**
     * @var \DateTime
     */
    private $dateEnd;
    /**
     * @var \DateTime
     */
    private $dateToday;
    /**
     * @var \DateInterval|false
     */
    private $diffEnd;
    /**
     * @var \DateInterval|false
     */
    private $diffStart;


    function __construct(\DateTime $dateStart, \DateTime $dateEnd)
    {
        $this->dateStart = $dateStart;
        $this->dateEnd = $dateEnd;
        $this->dateToday = new \DateTime();
        $this->diffStart = $this->dateToday->diff($this->dateStart);
        $this->diffEnd = $this->dateToday->diff($this->dateEnd);
    }

    /**
     * минимальный срок доставки
     * @return int
     */
    function getDayMin(){
        return $this->diffStart->d;
    }

    /**
     * максимальный срок доставки
     * @return int
     */
    function getDayMax(){
        return $this->diffEnd->d;
    }

    /**
     * Метод который возвращает интервал в днях ввиде строки.
     *
     * @param string $dayWord
     * @return string
     */
    function getDayInterval($dayWord = ' day')
    {
        $dayStart = $this->getDayStart();
        $dayEnd = $this->getDayEnd();


        if($dayStart == $dayEnd){
            return  $dayStart .$dayWord;
        }


        return $dayStart . '-' . $dayEnd . '' . $dayWord;
    }

    private function getDayStart(){
       return $this->getDayMin() ? $this->getDayMin() : 1;
    }
    private function getDayEnd(){
       return$this->getDayMax() ? $this->getDayMax() : 1;
    }


}