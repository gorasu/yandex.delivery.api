<?php


namespace GoraSu\Components\YandexDeliveryApi\Request\Params;


use GoraSu\Components\YandexDeliveryApi\Request\Type\DeliveryOptions\Cost;
use GoraSu\Components\YandexDeliveryApi\Request\Type\DeliveryOptions\Dimensions;
use GoraSu\Components\YandexDeliveryApi\Request\Type\DeliveryOptions\Place;
use GoraSu\Components\YandexDeliveryApi\Request\Type\DeliveryOptions\ToAddress;
use JMS\Serializer\Annotation AS JMS;

class DeliveryOptionsParams
{

    const CLASS_TO_ADDRESS = ToAddress::class;
    const CLASS_COST = Cost::class;
    const CLASS_DIMENSIONS = Dimensions::class;
    const CLASS_PLACE = "array<GoraSu\Components\YandexDeliveryApi\Request\Type\DeliveryOptions\Place>";

    /**
     * @JMS\Type("int")
     */
    private $senderId;


    /**
     * @JMS\Type("string")
     */
    private $deliveryType;
    /**
     * @JMS\Type(DeliveryOptionsParams::CLASS_DIMENSIONS);
     */
    private $dimensions;

    /**
     * @JMS\Type(DeliveryOptionsParams::CLASS_TO_ADDRESS);
     */
    private $to;

    /**
     * @JMS\Type(DeliveryOptionsParams::CLASS_COST);
     */
    private $cost;

    /**
     * Грузо места, расчитываются на основе данны товара
     * максимальный общий объем грузо места 500х500х500 1500кг
     * @var array<Place>
     * @JMS\Type(DeliveryOptionsParams::CLASS_PLACE)
     */
    private $places;


    function __construct()
    {

        $this->to = new ToAddress();
        //$this->dimensions = new Dimensions();
        $this->cost = new Cost();


    }

    /**
     * @param int $senderId
     */
    public function setSenderId($senderId)
    {
        $this->senderId = $senderId;
    }

    /**
     * @param string $deliveryType
     */
    public function setDeliveryType($deliveryType)
    {
        $this->deliveryType = $deliveryType;
    }

    /**
     * @return Cost
     */
    public function cost()
    {
        return $this->cost;
    }


    /**
     * @return int
     */
    public function getSenderId()
    {
        return $this->senderId;
    }

    /**
     * @return string
     */
    public function getDeliveryType()
    {
        return $this->deliveryType;
    }

    /**
     * @return Dimensions
     * @deprecated
     */
    public function dimensions()
    {
        if(!$this->dimensions){
            $this->dimensions =  new Dimensions();
        }
        return $this->dimensions;
    }

    /**
     * @return ToAddress
     */
    public function to()
    {
        return $this->to;
    }

    /**
     * @return Place[]
     */
    public function getPlaces()
    {
        return $this->places;
    }

    /**
     * @param Place $places
     * @return DeliveryOptionsParams
     */
    public function addPlaces(Place  $place)
    {
        $this->places[] = $place;
        return $this;
    }



    /**
     * @return array
     * @deprecated
     */
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}