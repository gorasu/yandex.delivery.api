<?php


namespace GoraSu\Components\YandexDeliveryApi\Request\Params;


use GoraSu\Components\YandexDeliveryApi\Request\Type\OrderDraft\Contact;
use GoraSu\Components\YandexDeliveryApi\Request\Type\OrderDraft\Cost;
use GoraSu\Components\YandexDeliveryApi\Request\Type\OrderDraft\DeliveryOption;
use GoraSu\Components\YandexDeliveryApi\Request\Type\OrderDraft\Dimensions;
use GoraSu\Components\YandexDeliveryApi\Request\Type\OrderDraft\Item;
use GoraSu\Components\YandexDeliveryApi\Request\Type\OrderDraft\Place;
use GoraSu\Components\YandexDeliveryApi\Request\Type\OrderDraft\Recipient;
use GoraSu\Components\YandexDeliveryApi\Request\Type\OrderDraft\Shipment;
use GoraSu\Components\YandexDeliveryApi\Type\Measure\Height;
use GoraSu\Components\YandexDeliveryApi\Type\Measure\Length;
use GoraSu\Components\YandexDeliveryApi\Type\Measure\Weight;
use GoraSu\Components\YandexDeliveryApi\Type\Measure\Width;
use JMS\Serializer\Annotation AS JMS;

/**
 * Параметры для запроса создания черновика заказа
 * Class OrderDraftParams
 * @package GoraSu\Components\YandexDeliveryApi\Request\Params
 */
class OrderDraftParams
{

    const CLASS_RECIPIENT = Recipient::class;
    const CLASS_COST = Cost::class;
    const CLASS_CONTACT = 'array<GoraSu\Components\YandexDeliveryApi\Request\Type\OrderDraft\Contact>';
    const CLASS_DELIVERY_OPTION = DeliveryOption::class;
    const CLASS_SHIPMENT = Shipment::class;
    const CLASS_PLACES = 'array<GoraSu\Components\YandexDeliveryApi\Request\Type\OrderDraft\Place>';

    const DELIVERY_TYPE_COURIER = 'COURIER';
    const DELIVERY_TYPE_PICKUP = 'PICKUP';
    const DELIVERY_TYPE_POST = 'POST';


    /**
     * @JMS\Type("int")
     */
    private $senderId;

    /**
     * @var string
     * @JMS\Type("string")
     */
    private $deliveryType;

    /**
     * Комментарий.
     * @var string
     * @JMS\Type("string")
     */
    private $comment;

    /**
     * Идентификатор заказа в системе партнера.
     * Может содержать прописные и строчные латинские буквы, цифры, знак «минус», знак нижнего подчеркивания, прямой и обратный слеш.
     * Длина идентификатора — от 1 до 10 символов.
     * @var string
     * @JMS\Type("string")
     */
    private $externalId;

    /**
     * @var Recipient
     * @JMS\Type(OrderDraftParams::CLASS_RECIPIENT)
     */
     private $recipient;

    /**
     * @var Cost
     * @JMS\Type(OrderDraftParams::CLASS_COST)
     */
     private $cost;

    /**
     * @var Contact[]
     * @JMS\Type(OrderDraftParams::CLASS_CONTACT)
     */
     private $contacts;

    /**
     * @var DeliveryOption
     * @JMS\Type(OrderDraftParams::CLASS_DELIVERY_OPTION)
     */
     private $deliveryOption;

    /**
     * @var Shipment
     * @JMS\Type(OrderDraftParams::CLASS_SHIPMENT)
     */
     private $shipment;

     /**
      * Грузо места, расчитываются на основе данны товара
      * максимальный общий объем грузо места 500х500х500 1500кг
     * @var array<Place>
     * @JMS\Type(OrderDraftParams::CLASS_PLACES)
     */
    private $places;


    /**
     * @var array<Item>
     * @JMS\Type("array<GoraSu\Components\YandexDeliveryApi\Request\Type\OrderDraft\Item>")
     */
    private $items = [];







    /**
     * OrderDraftParams constructor.
     * @param int $senderId
     * @param string 'COURIER'|'PICKUP'|'POST' $deliveryType
     */
    function __construct($senderId, $deliveryType) {


        $this->senderId = $senderId;
        $this->deliveryType = $deliveryType;
    }


    /**
     * @return int
     */
    public function getSenderId()
    {
        return $this->senderId;
    }

    /**
     * @return string
     */
    public function getDeliveryType()
    {
        return $this->deliveryType;
    }

    /**
     * @return Place[]
     */
    public function getPlaces()
    {
        return $this->places;
    }

    /**
     * @return Shipment
     */
    public function getShipment()
    {
        return $this->shipment;
    }

    /**
     * @param Shipment $shipment
     * @return OrderDraftParams
     */
    public function setShipment(Shipment $shipment)
    {
        $this->shipment = $shipment;
        return $this;
    }

    /**
     * @return DeliveryOption
     */
    public function getDeliveryOption()
    {
        return $this->deliveryOption;
    }

    /**
     * @param DeliveryOption $deliveryOption
     * @return OrderDraftParams
     */
    public function setDeliveryOption(DeliveryOption $deliveryOption)
    {
        $this->deliveryOption = $deliveryOption;
        return $this;
    }

    /**
     * @param Contact $contact
     * @return $this
     */
    public function addContact(Contact $contact){
        $this->contacts[] = $contact;
        return $this;
    }

    /**
     * @return Contact[]
     */
    public function getContacts()
    {
        return $this->contacts;
    }

    /**
     * @param Contact[] $contacts
     * @return OrderDraftParams
     */
    public function setContacts(array $contacts)
    {
        $this->contacts = $contacts;
        return $this;
    }

    /**
     * @return Cost
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * @param Cost $cost
     * @return OrderDraftParams
     */
    public function setCost(Cost $cost)
    {
        $this->cost = $cost;
        return $this;
    }

    /**
     * @return Recipient
     */
    public function getRecipient()
    {
        return $this->recipient;
    }

    /**
     * @param Recipient $recipient
     * @return OrderDraftParams
     */
    public function setRecipient($recipient)
    {
        $this->recipient = $recipient;
        return $this;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     * @return OrderDraftParams
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
        return $this;
    }

    /**
     * @return string
     */
    public function getExternalId()
    {
        return $this->externalId;
    }

    /**
     * @param string $externalId
     * @return OrderDraftParams
     */
    public function setExternalId($externalId)
    {
        $this->externalId = $externalId;
        return $this;
    }



    /**
     * Добавление товара
     * При добавлении товара в запрос будет добавлено грузо место на основе габаритов товара
     *
     * @param Item $item
     * @return $this
     *
     */
    function addProductItem(Item $item){

        for ($i=0; $i < $item->getCount(); $i++) {
            $place = new Place();
            $place->setDimensions($item->getDimensions());
            $this->places[] = $place;
        }
        $this->items[] = $item;
        return $this;
    }

    /**
     * Добавление товаров
     * При инициализации нового списка товаров должны очистится места которые ранее были добавлены в запрос
     *
     * @param Item[] $item
     * @return $this
     *
     */
    function setProductItem( $items){

        $this->places = [];
        $this->items = [];
        foreach ($items as $item) {
            $this->addProductItem($item);
        }
        return $this;
    }








}