<?php


namespace GoraSu\Components\YandexDeliveryApi\Request\Params;


use GoraSu\Components\YandexDeliveryApi\Request\Type\OrderDraft\Contact;
use GoraSu\Components\YandexDeliveryApi\Request\Type\OrderDraft\Cost;
use GoraSu\Components\YandexDeliveryApi\Request\Type\OrderDraft\DeliveryOption;
use GoraSu\Components\YandexDeliveryApi\Request\Type\OrderDraft\Dimensions;
use GoraSu\Components\YandexDeliveryApi\Request\Type\OrderDraft\Item;
use GoraSu\Components\YandexDeliveryApi\Request\Type\OrderDraft\Place;
use GoraSu\Components\YandexDeliveryApi\Request\Type\OrderDraft\Recipient;
use GoraSu\Components\YandexDeliveryApi\Request\Type\OrderDraft\Shipment;
use GoraSu\Components\YandexDeliveryApi\Request\Type\PickupPoints\RangeLatitude;
use GoraSu\Components\YandexDeliveryApi\Type\Measure\Height;
use GoraSu\Components\YandexDeliveryApi\Type\Measure\Length;
use GoraSu\Components\YandexDeliveryApi\Type\Measure\Weight;
use GoraSu\Components\YandexDeliveryApi\Type\Measure\Width;
use JMS\Serializer\Annotation AS JMS;

/**
 * Параметры для запроса создания черновика заказа
 * Class OrderDraftParams
 * @package GoraSu\Components\YandexDeliveryApi\Request\Params
 */
class PickupPointsParams
{

    const CLASS_RANGE_LATITUDE = RangeLatitude::class;

    const TYPE_PICKUP_POINT = 'PICKUP_POINT';
    const TYPE_TERMINAL = 'TERMINAL';
    const TYPE_POST_OFFICE = 'POST_OFFICE';

    /**
     * Идентификаторы пунктов выдачи (для типов доставки — в пункт выдачи и на почту).
     * @JMS\Type("array<int>")
     */
    private $pickupPointIds;

    /**
     * Диапазон широты.
     * @var RangeLatitude
     * @JMS\Type(PickupPointsParams::CLASS_RANGE_LATITUDE)
     */
    private $latitude;

    /**
     * Диапазон долготы.
     * @var RangeLatitude
     * @JMS\Type(PickupPointsParams::CLASS_RANGE_LATITUDE)
     */
    private $longitude;


    /**
     * Идентификатор населенного пункта.
     * @var integer
     * @JMS\Type("int")
     */
    private $locationId;

    /**
     * Тип пункта выдачи заказов:
     * PICKUP_POINT — пункт выдачи.
     * TERMINAL — постамат.
     * POST_OFFICE — почтовое отделение
     * @var string 'PICKUP_POINT'|'TERMINAL'|'POST_OFFICE'
     * @JMS\Type("string")
     */
    private $type;



    /**
     * PickupPointsParams constructor.
     *
     *
     */
    function __construct() {

    }

    /**
     * @return mixed
     */
    public function getPickupPointIds()
    {
        return $this->pickupPointIds;
    }

    /**
     * @param mixed $pickupPointIds
     * @return PickupPointsParams
     */
    public function setPickupPointIds($pickupPointIds)
    {
        $this->pickupPointIds = $pickupPointIds;
        return $this;
    }

    /**
     * @return RangeLatitude
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * @param RangeLatitude $latitude
     * @return PickupPointsParams
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
        return $this;
    }

    /**
     * @return RangeLatitude
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @param RangeLatitude $longitude
     * @return PickupPointsParams
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;
        return $this;
    }

    /**
     * @return int
     */
    public function getLocationId()
    {
        return $this->locationId;
    }

    /**
     * @param int $locationId
     * @return PickupPointsParams
     */
    public function setLocationId($locationId)
    {
        $this->locationId = $locationId;
        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return PickupPointsParams
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }










}