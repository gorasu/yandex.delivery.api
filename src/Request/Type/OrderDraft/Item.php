<?php


namespace GoraSu\Components\YandexDeliveryApi\Request\Type\OrderDraft;

use JMS\Serializer\Annotation AS JMS;


/**
 * Данные о товарах в заказе.
 * Class Item
 * @package GoraSu\Components\YandexDeliveryApi\Request\Type\OrderDraft
 */
class Item
{

    const VAT_20 = 'VAT_20';
    const VAT_10 = 'VAT_10';
    const VAT_0 = 'VAT_0';
    const NO_VAT = 'NO_VAT';
    const CLASS_DIMENSIONS = 'GoraSu\Components\YandexDeliveryApi\Request\Type\OrderDraft\Dimensions';


    /**
     * Идентификатор товара в системе партнера.
     * @var string
     * @JMS\Type("string")
     */
    private $externalId;

    /**
     * Вес и габариты отправления
     * @var Dimensions
     * @JMS\Type(Item::CLASS_DIMENSIONS)
     */
    private $dimensions;

    /**
     * Название товара.
     * @var string
     * @JMS\Type("string")
     */
    private $name;

    /**
     * Количество единиц товара
     * @var int
     * @JMS\Type("int")
     */
    private $count;

    /**
     * Цена товара в рублях.
     * Значение округляется до копеек (двух знаков после разделителя) по правилам математического округления. Например, 100.555 округлится до 100.56
     * @var double
     * @JMS\Type("double")
     */
    private $price;

    /**
     * Объявленная стоимость товара в рублях.
     * Значение округляется до копеек (двух знаков после разделителя) по правилам математического округления. Например, 100.555 округлится до 100.56.
     * @var double
     * @JMS\Type("double")
     */
    private $assessedValue;

    /**
     * Вид налогообложения товара:
     * VAT_20 — НДС 20%.
     * VAT_10 — НДС 10%.
     * VAT_0 — НДС 0%.
     * NO_VAT — не облагается НДС.
     * @var string 'VAT_20'|'VAT_10'|'VAT_0'|'NO_VAT'
     * @JMS\Type("string")
     */
    private $tax;


    /**
     * Item constructor.
     * @param $name
     * @param $price
     * @param $count
     * @param string 'VAT_20'|'VAT_10'|'VAT_0'|'NO_VAT $tax
     */
    function __construct($name, $price, $count, $tax){

        $this->name = $name;
        $this->price = $this->roundPrice($price);
        $this->count = $count;
        $this->setTax($tax);

    }

    private function setTax($tax){
        $possibleTax = [self::NO_VAT, self::VAT_0, self::VAT_10, self::VAT_20];
        if(!in_array($tax, $possibleTax)){
            throw new \DomainException('Tax mast be one of '.implode(', ',$possibleTax));
        }
        $this->tax = $tax;
    }

    /**
     * @return string
     */
    public function getExternalId()
    {
        return $this->externalId;
    }

    /**
     * @param string $externalId
     * @return Item
     */
    public function setExternalId($externalId)
    {
        $this->externalId = $externalId;
        return $this;
    }

    /**
     * @return Dimensions
     */
    public function getDimensions()
    {
        return $this->dimensions;
    }

    /**
     * @param Dimensions $dimensions
     * @return Item
     */
    public function setDimensions(Dimensions $dimensions)
    {
        $this->dimensions = $dimensions;
        return $this;
    }

    /**
     * @return int
     */
    public function getAssessedValue()
    {
        return $this->assessedValue;
    }

    /**
     * @param int $assessedValue
     * @return Item
     */
    public function setAssessedValue($assessedValue)
    {
        $this->assessedValue = $this->roundPrice($assessedValue);
        return $this;
    }

    /**
     * @return string 'VAT_20'|'VAT_10'|'VAT_0'|'NO_VAT'
     */
    public function getTax()
    {
        return $this->tax;
    }



    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }




    private function roundPrice($price){

        return round($price,2);

    }

}