<?php


namespace GoraSu\Components\YandexDeliveryApi\Request\Type\OrderDraft;

use JMS\Serializer\Annotation AS JMS;


/**
 * Class DeliveryOption
 * @package GoraSu\Components\YandexDeliveryApi\Request\Type\OrderDraft
 */
class DeliveryOption
{

    const CLASS_SERVICES = 'array<GoraSu\Components\YandexDeliveryApi\Request\Type\OrderDraft\Services>';
    /**
     * Идентификатор тарифа.
     * @var string
     * @JMS\Type("int")
     */
    private $tariffId;
    /**
     *
     * Стоимость доставки.
     * Значение округляется до копеек (двух знаков после разделителя) по правилам математического округления. Например, 100.555 округлится до 100.56.
     *
     * @var double
     * @JMS\Type("double")
     */
    private $delivery;

    /**
     * Стоимость доставки для покупателя.
     * Значение округляется до копеек (двух знаков после разделителя) по правилам математического округления. Например, 100.555 округлится до 100.56.
     * @var double
     * @JMS\Type("double")
     */
    private $deliveryForCustomer;
    /**
     * Начальная дата интервала доставки, рассчитанного сервисом.
     * Дата указывается в формате YYYY-MM-DD, входит в интервал.
     * @var \DateTime
     * @JMS\Type("DateTime<'Y-m-d'>")
     *
     */
    private $calculatedDeliveryDateMin;
    /**
     * Конечная дата интервала доставки, рассчитанного сервисом.
     * Дата указывается в формате YYYY-MM-DD, входит в интервал.
     * @var \DateTime
     * @JMS\Type("DateTime<'Y-m-d'>")
     */

    private $calculatedDeliveryDateMax;
    /**
     * Идентификатор интервала времени, в который нужно доставить заказ покупателю.
     * @var int
     * @JMS\Type("int")
     */
    private $deliveryIntervalId;
    /**
     * Данные о дополнительных услугах доставки.
     * @var Services
     * @JMS\Type(DeliveryOption::CLASS_SERVICES)
     */
    private $services =[];
    /**
     * Идентификатор службы доставки.
     * @var int
     * @JMS\Type("int")
     */
    private $partnerId;


    /**
     * @param Services $services
     * @return $this
     */
    function addService(Services $services){

        $this->services[] = $services;
        return $this;

    }

    /**
     * @return string
     */
    public function getTariffId()
    {
        return $this->tariffId;
    }

    /**
     * @param string $tariffId
     * @return DeliveryOption
     */
    public function setTariffId($tariffId)
    {
        $this->tariffId = $tariffId;
        return $this;
    }

    /**
     * @return float
     */
    public function getDelivery()
    {
        return $this->delivery;
    }

    /**
     * @param float $delivery
     * @return DeliveryOption
     */
    public function setDelivery($delivery)
    {
        $this->delivery = $this->round($delivery);
        return $this;
    }

    /**
     * @return float
     */
    public function getDeliveryForCustomer()
    {
        return $this->deliveryForCustomer;
    }

    /**
     * @param float $deliveryForCustomer
     * @return DeliveryOption
     */
    public function setDeliveryForCustomer($deliveryForCustomer)
    {
        $this->deliveryForCustomer = $this->round($deliveryForCustomer);
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCalculatedDeliveryDateMin()
    {
        return $this->calculatedDeliveryDateMin;
    }

    /**
     * @param \DateTime $calculatedDeliveryDateMin
     * @return DeliveryOption
     */
    public function setCalculatedDeliveryDateMin($calculatedDeliveryDateMin)
    {
        $this->calculatedDeliveryDateMin = $calculatedDeliveryDateMin;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCalculatedDeliveryDateMax()
    {
        return $this->calculatedDeliveryDateMax;
    }

    /**
     * @param \DateTime $calculatedDeliveryDateMax
     * @return DeliveryOption
     */
    public function setCalculatedDeliveryDateMax($calculatedDeliveryDateMax)
    {
        $this->calculatedDeliveryDateMax = $calculatedDeliveryDateMax;
        return $this;
    }

    /**
     * @return int
     */
    public function getDeliveryIntervalId()
    {
        return $this->deliveryIntervalId;
    }

    /**
     * @param int $deliveryIntervalId
     * @return DeliveryOption
     */
    public function setDeliveryIntervalId($deliveryIntervalId)
    {
        $this->deliveryIntervalId = $deliveryIntervalId;
        return $this;
    }

    /**
     * @return Services
     */
    public function getServices()
    {
        return $this->services;
    }

    /**
     * @param Services $services
     * @return DeliveryOption
     */
    public function setServices(array $services)
    {
        $this->services = $services;
        return $this;
    }

    /**
     * @return int
     */
    public function getPartnerId()
    {
        return $this->partnerId;
    }

    /**
     * @param int $partnerId
     * @return DeliveryOption
     */
    public function setPartnerId($partnerId)
    {
        $this->partnerId = $partnerId;
        return $this;
    }


    protected function round($price){
        return round($price, 2);
    }


}