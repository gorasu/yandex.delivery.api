<?php


namespace GoraSu\Components\YandexDeliveryApi\Request\Type\OrderDraft;

use JMS\Serializer\Annotation AS JMS;


/**
 * Данные о получателе
 * Class Recipient
 * @package GoraSu\Components\YandexDeliveryApi\Request\Type\OrderDraft
 */
class Recipient
{

    const CLASS_ADDRESS = Address::class;

    /**
     * Имя получателя.
     * @var string
     * @JMS\Type("string")
     */
    private $firstName;
    /**
     * Отчество получателя.
     * @var string
     * @JMS\Type("string")
     */
    private $middleName;
    /**
     * Фамилия получателя.
     * @var string
     * @JMS\Type("string")
     */
    private $lastName;
    /**
     * Адрес электронной почты получателя.
     * @var string
     * @JMS\Type("string")
     */
    private $email;
    /**
     * Адрес получателя.
     * @var Address
     * @JMS\Type(Recipient::CLASS_ADDRESS)
     */
    private $address;
    /**
     * Адрес получателя в произвольном формате. При указанном fullAddress параметр address игнорируется. Если не удастся извлечь из значения параметра точный адрес, вы получите ошибку.
     * @var string
     * @JMS\Type("string")
     */
    private $fullAddress;
    /**
     * Идентификатор пункта выдачи (для типов доставки — в пункт выдачи и на почту).
     * @var int
     * @JMS\Type("int")
     */
    private $pickupPointId;

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     * @return Recipient
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * @return string
     */
    public function getMiddleName()
    {
        return $this->middleName;
    }

    /**
     * @param string $middleName
     * @return Recipient
     */
    public function setMiddleName($middleName)
    {
        $this->middleName = $middleName;
        return $this;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     * @return Recipient
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return Recipient
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return Address
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param Address $address
     * @return Recipient
     */
    public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return string
     */
    public function getFullAddress()
    {
        return $this->fullAddress;
    }

    /**
     * @param string $fullAddress
     * @return Recipient
     */
    public function setFullAddress($fullAddress)
    {
        $this->fullAddress = $fullAddress;
        return $this;
    }

    /**
     * @return int
     */
    public function getPickupPointId()
    {
        return $this->pickupPointId;
    }

    /**
     * @param int $pickupPointId
     * @return Recipient
     */
    public function setPickupPointId($pickupPointId)
    {
        $this->pickupPointId = $pickupPointId;
        return $this;
    }



}