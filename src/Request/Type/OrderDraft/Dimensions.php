<?php


namespace GoraSu\Components\YandexDeliveryApi\Request\Type\OrderDraft;

use GoraSu\Components\YandexDeliveryApi\Request\Type\AbstractDimensions;

/**
 * Вес и габариты отправления
 * Class Dimensions
 * @package GoraSu\Components\YandexDeliveryApi\Request\Type\OrderDraft
 */
class Dimensions extends AbstractDimensions
{


}