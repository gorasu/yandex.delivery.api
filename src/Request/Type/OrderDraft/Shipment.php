<?php


namespace GoraSu\Components\YandexDeliveryApi\Request\Type\OrderDraft;

use JMS\Serializer\Annotation AS JMS;


/**
 * Class Shipment
 * @package GoraSu\Components\YandexDeliveryApi\Request\Type\OrderDraft
 */
class Shipment
{
    /**
     * Тип отгрузки:
     * IMPORT — самостоятельно.
     * WITHDRAW — курьером.
     * @var string
     * @JMS\Type("string")
     */
    private $type;
    /**
     * Дата отгрузки в формате YYYY-MM-DD
     * @var \DateTime
     * @JMS\Type("DateTime<'Y-m-d'>")
     */
    private $date;
    /**
     * Идентификатор склада, с которого отгружаются товары.
     * @var int
     * @JMS\Type("int")
     */
    private $warehouseFrom;
    /**
     * Идентификатор склада, на который отгружаются товары.
     * @var int
     * @JMS\Type("int")
     */
    private $warehouseTo;
    /**
     * Идентификатор партнера, которому отгружаются товары.
     * @var int
     * @JMS\Type("int")
     */
    private $partnerTo;

    /**
     * Shipment constructor.
     * @param string 'IMPORT'|'WITHDRAW' $type
     */
    function __construct($type)
    {
        $this->type = $type;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     * @return Shipment
     */
    public function setDate( \DateTime $date)
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return int
     */
    public function getWarehouseFrom()
    {
        return $this->warehouseFrom;
    }

    /**
     * @param int $warehouseFrom
     * @return Shipment
     */
    public function setWarehouseFrom($warehouseFrom)
    {
        $this->warehouseFrom = $warehouseFrom;
        return $this;
    }

    /**
     * @return int
     */
    public function getWarehouseTo()
    {
        return $this->warehouseTo;
    }

    /**
     * @param int $warehouseTo
     * @return Shipment
     */
    public function setWarehouseTo($warehouseTo)
    {
        $this->warehouseTo = $warehouseTo;
        return $this;
    }

    /**
     * @return int
     */
    public function getPartnerTo()
    {
        return $this->partnerTo;
    }

    /**
     * @param int $partnerTo
     * @return Shipment
     */
    public function setPartnerTo($partnerTo)
    {
        $this->partnerTo = $partnerTo;
        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }



}