<?php


namespace GoraSu\Components\YandexDeliveryApi\Request\Type\OrderDraft;

use JMS\Serializer\Annotation AS JMS;

/**
 * Class Services
 * @package GoraSu\Components\YandexDeliveryApi\Request\Type\OrderDraft
 */
class Services
{

    /**
     * Код услуги.
     * Возможные значения:
     * DELIVERY — доставка.
     * CASH_SERVICE — вознаграждение за перечисление денежных средств.
     * SORT — сортировка на едином складе.
     * INSURANCE — объявление ценности заказа.
     * WAIT_20 — ожидание курьера.
     * RETURN — возврат заказа на единый склад.
     * RETURN_SORT — сортировка возвращенного заказа.
     * @var string
     * @JMS\Type("string")
     */
    private $code;

    /**
     * @var double
     * @JMS\Type("double")
     */
    private $cost;
    /**
     * Услуга оплачивается клиентом.
     * Возможные значения:
     * true — услуга оплачивается клиентом.
     * false — услуга не оплачивается клиентом
     * @var bool
     * @JMS\Type("bool")
     */
    private $customerPay;

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string 'DELIVERY'|'CASH_SERVICE'|'SORT'|'INSURANCE'|'WAIT_20|'RETURN'|'RETURN_SORT' $code
     * @return Services
     */
    public function setCode($code)
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return float
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * @param float $cost
     * @return Services
     */
    public function setCost($cost)
    {
        $this->cost = $this->round($cost);
        return $this;
    }

    /**
     * @return bool
     */
    public function isCustomerPay()
    {
        return $this->customerPay;
    }

    /**
     * @param bool $customerPay
     * @return Services
     */
    public function setCustomerPay($customerPay)
    {
        $this->customerPay = $customerPay;
        return $this;
    }


    protected function round($price){
        return round($price, 2);
    }


}