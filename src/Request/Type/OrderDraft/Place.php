<?php


namespace GoraSu\Components\YandexDeliveryApi\Request\Type\OrderDraft;

use JMS\Serializer\Annotation AS JMS;


/**
 * Данные о грузовых местах заказа.
 * Class Place
 * @package GoraSu\Components\YandexDeliveryApi\Request\Type\OrderDraft
 */
class Place
{

    /**
     * Идентификатор грузового места в системе партнера.
     * @var string
     * @JMS\Type("string")
     */
    private $externalId;
    /**
     * Вес и габариты отправления.
     * @var Dimensions
     * @JMS\Type("GoraSu\Components\YandexDeliveryApi\Request\Type\OrderDraft\Dimensions")
     */
    private $dimensions;




    /**
     * @return string
     */
    public function getExternalId()
    {
        return $this->externalId;
    }

    /**
     * @param string $externalId
     * @return Place
     */
    public function setExternalId($externalId)
    {
        $this->externalId = $externalId;
        return $this;
    }

    /**
     * @return Dimensions
     */
    public function getDimensions()
    {
        return $this->dimensions;
    }

    /**
     * @param Dimensions $dimensions
     * @return Place
     */
    public function setDimensions(Dimensions $dimensions)
    {
        $this->dimensions = $dimensions;
        return $this;
    }



    /**
     * @param Item[] $items
     * @return Place
     */
    public function setItems(array $items)
    {
        $this->items = $items;
        return $this;
    }


}