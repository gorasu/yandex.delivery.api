<?php


namespace GoraSu\Components\YandexDeliveryApi\Request\Type;

use GoraSu\Components\YandexDeliveryApi\Type\Measure\Height;
use GoraSu\Components\YandexDeliveryApi\Type\Measure\Length;
use GoraSu\Components\YandexDeliveryApi\Type\Measure\Weight;
use GoraSu\Components\YandexDeliveryApi\Type\Measure\Width;
use JMS\Serializer\Annotation as JMS;


/**
 * Абстратный тип запроса габаритов, для снижения дублирования кода
 * Данный класс является родительским для типа Dimensions в конкретном запросе
 *
 * Class AbstractDimensions
 * @package GoraSu\Components\YandexDeliveryApi\Request\Type
 */
abstract class AbstractDimensions
{
    /**
     * @JMS\Type("int")
     * @var integer
     */
    private $length;
    /**
     * @JMS\Type("int")
     * @var integer
     */
    private $height;
    /**
     * @JMS\Type("int")
     * @var integer
     */
    private $width;
    /**
     * @JMS\Type("float")
     * @var integer | float
     */
    private $weight;


    private function __construct()
    {


    }


    /**
     * @param int $length
     * @return $this
     */
    private function setLength(Length $length)
    {
        $this->length = $length->getInCm();
        return $this;
    }

    /**
     * @param int $height
     * @return $this
     */
    private function setHeight(Height $height)
    {
        $this->height = $height->getInCm();
        return $this;
    }

    /**
     * @param int $width
     * @return $this
     */
    private function setWidth(Width $width)
    {
        $this->width = $width->getInCm();
        return $this;
    }

    /**
     * @param int $weight
     * @return $this
     */
    private function setWeight(Weight $weight)
    {
        $this->weight = $weight->getKilogram();
        return $this;
    }


    /**
     * @param Width $width
     * @param Height $height
     * @param Length $length
     * @param Weight $weight
     * @return $this
     */
    public static function create(Width $width, Height $height, Length $length, Weight $weight){

        $dimensions = new static();
        $dimensions->setWidth($width);
        $dimensions->setHeight($height);
        $dimensions->setLength($length);
        $dimensions->setWeight($weight);


        return $dimensions;

    }

    /**
     * @return int
     */
    public function getLength()
    {
        return $this->length;
    }

    /**
     * @return int
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @return int
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * @return float|int
     */
    public function getWeight()
    {
        return $this->weight;
    }




}