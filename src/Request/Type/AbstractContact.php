<?php


namespace GoraSu\Components\YandexDeliveryApi\Request\Type;

use JMS\Serializer\Annotation AS JMS;

/**
 * Класс для запроса данных контакта
 * Class AbstractContact
 * @package GoraSu\Components\YandexDeliveryApi\Request\Type
 */
abstract class AbstractContact
{

    /**
     * Тип контакта:
     * RECIPIENT
     * CREDENTIALS
     * CONTACT
     * @var string
     * @JMS\Type("string")
     */
    protected $type;
    /**
     * Номер телефона.
     * @var string
     * @JMS\Type("string")
     */
    protected $phone;
    /**
     * Дополнительный номер телефона.
     * @var string
     * @JMS\Type("string")
     */
    protected $additional;
    /**
     * Имя получателя.
     * @var string
     * @JMS\Type("string")
     */
    protected $firstName;
    /**
     * Отчество получателя.
     * @var string
     * @JMS\Type("string")
     */
    protected $middleName;
    /**
     * Фамилия получателя.
     * @var string
     * @JMS\Type("string")
     */
    protected $lastName;

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }


    /**
     * @param string 'RECIPIENT'|'CREDENTIALS'|'CONTACT' $type
     * @return $this
     *
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     * @return $this
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return string
     */
    public function getAdditional()
    {
        return $this->additional;
    }

    /**
     * @param string $additional
     * @return $this
     */
    public function setAdditional($additional)
    {
        $this->additional = $additional;
        return $this;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     * @return $this
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * @return string
     */
    public function getMiddleName()
    {
        return $this->middleName;
    }

    /**
     * @param string $middleName
     * @return AbstractContact
     */
    public function setMiddleName($middleName)
    {
        $this->middleName = $middleName;
        return $this;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     * @return AbstractContact
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
        return $this;
    }


    


}