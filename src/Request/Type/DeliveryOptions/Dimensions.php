<?php


namespace GoraSu\Components\YandexDeliveryApi\Request\Type\DeliveryOptions;

use JMS\Serializer\Annotation AS JMS;


class Dimensions
{
    /**
     * @JMS\Type("int")
     */
    private $length;
    /**
     * @JMS\Type("int")
     */
    private $height;
    /**
     * @JMS\Type("int")
     */
    private $width;
    /**
     * @JMS\Type("float")
     */
    private $weight;

    /**
     * @param int $length
     * @return Dimensions
     */
    public function setLength($length)
    {
        $this->length = $length;
        return $this;
    }

    /**
     * @param int $height
     * @return Dimensions
     */
    public function setHeight($height)
    {
        $this->height = $height;
        return $this;
    }

    /**
     * @param int $width
     * @return Dimensions
     */
    public function setWidth($width)
    {
        $this->width = $width;
        return $this;
    }

    /**
     * @param int $weight
     * @return Dimensions
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
        return $this;
    }



}