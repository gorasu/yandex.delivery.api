<?php


namespace GoraSu\Components\YandexDeliveryApi\Request\Type\DeliveryOptions;
use JMS\Serializer\Annotation AS JMS;


class Place
{


    CONST DIMENSIONS_CLASS = Dimensions::class;


    /**
     * Вес и габариты отправления.
     * @var Dimensions
     * @JMS\Type(Place::DIMENSIONS_CLASS)
     */
    private $dimensions;

    function __construct(Dimensions $dimensions){

        $this->dimensions = $dimensions;

    }

    /**
     * @return Dimensions
     */
    public function getDimensions()
    {
        return $this->dimensions;
    }






//"dimensions": {
//"length": {int},
//"width": {int},
//                "height": {int},
//                "weight": {double}
//              }

}