<?php


namespace GoraSu\Components\YandexDeliveryApi\Request\Type\DeliveryOptions;
use JMS\Serializer\Annotation AS JMS;


class ToAddress
{

    /**
     * @JMS\Type("string")
     */
    private $location;
    /**
     * @JMS\Type("int")
     */
    private $geoId;
    /**
     * @JMS\Type("int")
     */
    private $postalCode;
    /**
     * @JMS\Type("array<int>")
     */
    private $pickupPointIds;

    /**
     * @param string $location
     * @return ToAddress
     */
    public function setLocation($location)
    {
        $this->location = $location;
        return $this;
    }

    /**
     * @param int $geoId
     * @return ToAddress
     */
    public function setGeoId($geoId)
    {
        $this->geoId = $geoId;
        return $this;
    }

    /**
     * @param int $postalCode
     * @return ToAddress
     */
    public function setPostalCode($postalCode)
    {
        $this->postalCode = $postalCode;
        return $this;
    }

    /**
     * @param int[] $pickupPointIds
     * @return ToAddress
     */
    public function setPickupPointIds($pickupPointIds)
    {
        $this->pickupPointIds = $pickupPointIds;
        return $this;
    }



}