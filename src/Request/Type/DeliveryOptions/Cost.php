<?php


namespace GoraSu\Components\YandexDeliveryApi\Request\Type\DeliveryOptions;
use JMS\Serializer\Annotation AS JMS;


/**
 * Стоимость заказа
 * Class Cost
 * @package GoraSu\Components\YandexDeliveryApi\Request\Type
 */
class Cost
{

    /**
     * @JMS\Type("int")
     */
    private $assessedValue;
    /**
     * @JMS\Type("int")
     */
    private $itemsSum;
    /**
     * @JMS\Type("int")
     */
    private $manualDeliveryForCustomer;
    /**
     * @JMS\Type("bool")
     */
    private $fullyPrepaid;

    /**
     * @param int $assessedValue
     * @return Cost
     */
    public function setAssessedValue($assessedValue)
    {
        $this->assessedValue = $assessedValue;
        return $this;
    }

    /**
     * @param int $itemsSum
     * @return Cost
     */
    public function setItemsSum($itemsSum)
    {
        $this->itemsSum = $itemsSum;
        return $this;
    }

    /**
     * @param int $manualDeliveryForCustomer
     * @return Cost
     */
    public function setManualDeliveryForCustomer($manualDeliveryForCustomer)
    {
        $this->manualDeliveryForCustomer = $manualDeliveryForCustomer;
        return $this;
    }

    /**
     * @param bool $fullyPrepaid
     * @return Cost
     */
    public function setFullyPrepaid($fullyPrepaid)
    {
        $this->fullyPrepaid = $fullyPrepaid;
        return $this;
    }



}