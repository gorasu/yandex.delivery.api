<?php

namespace GoraSu\Components\YandexDeliveryApi\Request\Type\PickupPoints;

/**
 * Диапазон широт
 */
class RangeLatitude
{

    /**
     * От
     * @var string
     * @JMS\Type("double")
     */
    protected $from;
    /**
     * До
     * @var string
     * @JMS\Type("double")
     */
    protected $to;


    function __construct($from, $to){

        $this->from = $from;
        $this->to = $to;
    }

    /**
     * @return string
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * @return string
     */
    public function getTo()
    {
        return $this->to;
    }



}