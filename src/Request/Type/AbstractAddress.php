<?php


namespace GoraSu\Components\YandexDeliveryApi\Request\Type;

use JMS\Serializer\Annotation AS JMS;

/**
 * Класс адреса для запроса
 * Class AbstractContact
 * @package GoraSu\Components\YandexDeliveryApi\Request\Type
 */
abstract class AbstractAddress
{

    /**
     * Идентификатор населенного пункта.
     * @var int
     * @JMS\Type("int")
     */
    protected $geoId;
    /**
     * Страна
     * @var string
     * @JMS\Type("string")
     */
    protected $country;
    /**
     * Регион, область, округ.
     * @var string
     * @JMS\Type("string")
     */
    protected $region;
    /**
     * Субрегион.
     * @var string
     * @JMS\Type("string")
     */
    protected $subRegion;
    /**
     * Населенный пункт.
     * @var string
     * @JMS\Type("string")
     */
    protected $locality;
    /**
     * Улица
     * @var string
     * @JMS\Type("string")
     */
    protected $street;
    /**
     * Дом
     * @var string
     * @JMS\Type("string")
     */
    protected $house;
    /**
     * Корпус
     * @var string
     * @JMS\Type("string")
     */
    protected $housing;
    /**
     * Строение
     * @var string
     * @JMS\Type("string")
     */
    protected $building;
    /**
     * Номер квартиры или офиса.
     * @var string
     * @JMS\Type("string")
     */
    protected $apartment;
    /**
     * Почтовый индекс.
     * @var string
     * @JMS\Type("string")
     */
    protected $postalCode;
    /**
     * Почтовый индекс (устаревший элемент).
     *
     * @var string
     * @JMS\Type("string")
     */
    protected $postCode;

    /**
     * @return int
     */
    public function getGeoId()
    {
        return $this->geoId;
    }

    /**
     * @param int $geoId
     * @return AbstractAddress
     */
    public function setGeoId($geoId)
    {
        $this->geoId = $geoId;
        return $this;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $country
     * @return AbstractAddress
     */
    public function setCountry($country)
    {
        $this->country = $country;
        return $this;
    }

    /**
     * @return string
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * @param string $region
     * @return AbstractAddress
     */
    public function setRegion($region)
    {
        $this->region = $region;
        return $this;
    }

    /**
     * @return string
     */
    public function getSubRegion()
    {
        return $this->subRegion;
    }

    /**
     * @param string $subRegion
     * @return AbstractAddress
     */
    public function setSubRegion($subRegion)
    {
        $this->subRegion = $subRegion;
        return $this;
    }

    /**
     * @return string
     */
    public function getLocality()
    {
        return $this->locality;
    }

    /**
     * @param string $locality
     * @return AbstractAddress
     */
    public function setLocality($locality)
    {
        $this->locality = $locality;
        return $this;
    }

    /**
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @param string $street
     * @return AbstractAddress
     */
    public function setStreet($street)
    {
        $this->street = $street;
        return $this;
    }

    /**
     * @return string
     */
    public function getHouse()
    {
        return $this->house;
    }

    /**
     * @param string $house
     * @return AbstractAddress
     */
    public function setHouse($house)
    {
        $this->house = $house;
        return $this;
    }

    /**
     * @return string
     */
    public function getHousing()
    {
        return $this->housing;
    }

    /**
     * @param string $housing
     * @return AbstractAddress
     */
    public function setHousing($housing)
    {
        $this->housing = $housing;
        return $this;
    }

    /**
     * @return string
     */
    public function getBuilding()
    {
        return $this->building;
    }

    /**
     * @param string $building
     * @return AbstractAddress
     */
    public function setBuilding($building)
    {
        $this->building = $building;
        return $this;
    }

    /**
     * @return string
     */
    public function getApartment()
    {
        return $this->apartment;
    }

    /**
     * @param string $apartment
     * @return AbstractAddress
     */
    public function setApartment($apartment)
    {
        $this->apartment = $apartment;
        return $this;
    }

    /**
     * @return string
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * @param string $postalCode
     * @return AbstractAddress
     */
    public function setPostalCode($postalCode)
    {
        $this->postalCode = $postalCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getPostCode()
    {
        return $this->postCode;
    }

    /**
     * @param string $postCode
     * @return AbstractAddress
     */
    public function setPostCode($postCode)
    {
        $this->postCode = $postCode;
        return $this;
    }






}