<?php


namespace GoraSu\Components\YandexDeliveryApi\Request\Type;


use GoraSu\Components\YandexDeliveryApi\Request\Type\DeliveryOptions\Cost;

use JMS\Serializer\Annotation AS JMS;


abstract class AbstractCost
{

    /**
     * @var double
     * @JMS\Type("double")
     */
    protected $assessedValue;
    /**
     * Способ оплаты заказа:
     * CASH — наличными курьеру.
     * CARD — картой курьеру.
     * PREPAID — предоплата
     * @var string
     * @JMS\Type("string")
     */
    protected $paymentMethod;
    /**
     * Стоимость доставки для покупателя (определяется магазином).
     * Значение округляется до копеек (двух знаков после разделителя) по правилам математического округления. Например, 100.555 округлится до 100.56.
     * @var double
     * @JMS\Type("double")
     *
     */
    protected $manualDeliveryForCustomer;
    /**
     * @JMS\Type("bool")
     */
    protected $fullyPrepaid;

    /**
     * @param int $assessedValue
     * @return $this
     */
    public function setAssessedValue($assessedValue)
    {
        $this->assessedValue = $this->round($assessedValue);
        return $this;
    }

    /**
     * @param int $paymentMethod
     * @return $this
     */
    public function setPaymentMethod($paymentMethod)
    {
        $this->paymentMethod = $paymentMethod;
        return $this;
    }

    /**
     * @param int $manualDeliveryForCustomer
     * @return $this
     */
    public function setManualDeliveryForCustomer($manualDeliveryForCustomer)
    {
        $this->manualDeliveryForCustomer = $this->round($manualDeliveryForCustomer);
        return $this;
    }

    /**
     * @param bool $fullyPrepaid
     * @return $this
     */
    public function setFullyPrepaid($fullyPrepaid)
    {
        $this->fullyPrepaid = $fullyPrepaid;
        return $this;
    }


    protected function round($price){
        return round($price, 2);
    }

}