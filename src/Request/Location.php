<?php


namespace GoraSu\Components\YandexDeliveryApi\Request;



/**
 * Получение полного адреса
 * https://yandex.ru/dev/delivery-3/doc/dg/reference/get-location.html
 * Class Location
 * @package GoraSu\Components\YandexDeliveryApi\Request
 */
class Location implements RequestInterface
{

    private $term;

    function __construct($term){


        $this->term = $term;
    }


    function getHttpMethod()
    {
        return self::GET;
    }

    function getApiMethod()
    {
        return "location";
    }

    function getRequestParams()
    {
        return ["term"=>$this->term];
    }

    function getResponseClassName()
    {
        return null;
    }
}