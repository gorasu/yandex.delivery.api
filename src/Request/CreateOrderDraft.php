<?php


namespace GoraSu\Components\YandexDeliveryApi\Request;


use Exception;
use GoraSu\Components\YandexDeliveryApi\Request\Params\OrderDraftParams;
use GoraSu\Components\YandexDeliveryApi\Response\DeliveryOption;
use GoraSu\Components\YandexDeliveryApi\Response\OrderDraftResponse;

/**
 * Запрос на создание черновика зазказа
 * https://yandex.ru/dev/delivery-3/doc/dg/reference/post-orders.html
 *
 * Class CreateOrderDraft
 * @package GoraSu\Components\YandexDeliveryApi\Request
 */
class CreateOrderDraft implements RequestInterface
{

    /**
     * @var OrderDraftParams
     */
    private $orderDraftParams;

    function __construct(OrderDraftParams $orderDraftParams){

        $this->orderDraftParams = $orderDraftParams;
    }

    function getHttpMethod()
    {
        return self::POST;
    }

    function getApiMethod()
    {
        return 'orders';
    }

    function getRequestParams()
    {
        return $this->orderDraftParams;
    }

    function getResponseClassName()
    {
        return OrderDraftResponse::class;
        //return "array<".DeliveryOption::class.">";
    }
}