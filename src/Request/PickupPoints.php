<?php


namespace GoraSu\Components\YandexDeliveryApi\Request;



use GoraSu\Components\YandexDeliveryApi\Request\Params\PickupPointsParams;
use GoraSu\Components\YandexDeliveryApi\Response\PickupPointsResponse;

/**
 * Получение полного адреса
 * https://yandex.ru/dev/delivery-3/doc/dg/reference/get-location.html
 * Class Location
 * @package GoraSu\Components\YandexDeliveryApi\Request
 */
class PickupPoints implements RequestInterface
{


    /**
     * @var PickupPointsParams
     */
    private $params;

    function __construct(PickupPointsParams $params){


        $this->params = $params;
    }


    function getHttpMethod()
    {
        return self::PUT;
    }

    function getApiMethod()
    {
        return "pickup-points";
    }

    function getRequestParams()
    {
        return $this->params;
    }

    function getResponseClassName()
    {
        return "array<".PickupPointsResponse::class.">";
    }
}