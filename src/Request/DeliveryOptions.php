<?php


namespace GoraSu\Components\YandexDeliveryApi\Request;


use GoraSu\Components\YandexDeliveryApi\Request\Params\DeliveryOptionsParams;
use GoraSu\Components\YandexDeliveryApi\Response\DeliveryOption;

class DeliveryOptions implements RequestInterface
{

    /**
     * @var DeliveryOptionsParams
     */
    private $params;

    function __construct(DeliveryOptionsParams $params)
    {
        $this->params = $params;

    }

    function getHttpMethod()
    {
        return self::PUT;
    }

    function getApiMethod()
    {
        return "delivery-options";
    }

    function getRequestParams()
    {
        return $this->params;
    }

    function getResponseClassName()
    {
        return "array<".DeliveryOption::class.">";
    }
}