<?php


namespace GoraSu\Components\YandexDeliveryApi\Request;



/**
 * Интерфейс для отправки запросов в API
 * Interface RequestInterface
 * @package GoraSu\Components\YandexDeliveryApi\Request
 */
interface RequestInterface
{
    const POST = "POST";
    const PUT = "PUT";
    const GET = "GET";
    const DELETE = "DELETE";
    const PATCH = "PATCH";

    /**
     * Метод которым будет отправлен запрос (POST,GET,PUT,DELETE,PATCH)
     * @return mixed
     */
    function getHttpMethod();

    /**
     * Метод API который будет вызван при отправке запроса
     * @return mixed
     */
    function getApiMethod();

    /**
     * Параметры которые будут отправлены в теле запроса
     * @return mixed
     */
    function getRequestParams();

    /**
     * Название класса в который будет сконвертирован ответ от API
     * Если null то вернется ответ Json преобразованный в массив
     * @return string | null
     */
    function getResponseClassName();


}