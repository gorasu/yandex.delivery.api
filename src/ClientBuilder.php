<?php


namespace GoraSu\Components\YandexDeliveryApi;


use Doctrine\Common\Annotations\AnnotationRegistry;
use JMS\Serializer\Naming\IdenticalPropertyNamingStrategy;
use JMS\Serializer\Naming\SerializedNameAnnotationStrategy;
use JMS\Serializer\SerializerBuilder;

class ClientBuilder
{

    private $apiKey;

    function __construct($apiKey)
    {

        $this->apiKey = $apiKey;
    }

    function build(){


        AnnotationRegistry::registerLoader('class_exists');
        $serializer = SerializerBuilder::create()
            ->setPropertyNamingStrategy(
                new SerializedNameAnnotationStrategy(
                    new IdenticalPropertyNamingStrategy()
                )
            )
            ->build();
        return new Client($this->apiKey, new GuzzleBuilder(), $serializer);

    }


}