<?php


namespace GoraSu\Components\YandexDeliveryApi;


/**
 * Билдер Guzzle клиент
 * Class GuzzleBuilder
 * @package GoraSu\Components\YandexDeliveryApi
 */
class GuzzleBuilder
{

    function build($url, $authKey){

         return new \GuzzleHttp\Client([
            'base_uri' => $url,
            'headers' => [
                'Authorization' => 'OAuth '.$authKey,
                'User-Agent' => 'GoraSuYandexAPI/1.0'
            ]

        ]);

    }

}