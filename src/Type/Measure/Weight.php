<?php


namespace GoraSu\Components\YandexDeliveryApi\Type\Measure;


/**
 * Class Weight
 * @package GoraSu\Components\YandexDeliveryApi\Type\Measure
 */
class Weight
{


    /**
     * @var integer | float
     */
    private $kilogram;

    /**
     * Weight constructor.
     * @param $kilogram
     */
    private function __construct($kilogram){

        $this->kilogram = $kilogram;
    }


    /**
     * @param $gram
     * @return $this
     */
    static function createByGram($gram){

        return new static(($gram/1000));
    }

    /**
     * @param $kilogram
     * @return $this
     */
    static function createByKilogram($kilogram){

        return new static ($kilogram);

    }

    /**
     * @return integer | float
     */
    public function getKilogram(){
        return $this->kilogram;
    }

}