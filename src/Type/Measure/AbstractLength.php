<?php


namespace GoraSu\Components\YandexDeliveryApi\Type\Measure;


/**
 * Абстратный класс для управления длинами
 * Class AbstractLength
 * @package GoraSu\Components\YandexDeliveryApi\Type\Measure
 */
abstract  class AbstractLength
{
    /**
     * @var integer | float
     */
    private $centimeter;

    /**
     * Height constructor.
     * @param $centimeter
     */
    protected function __construct($centimeter){
        $this->centimeter = $centimeter;
    }

    /**
     * @param $centimeter
     * @return $this
     */
    public static function createByCm($centimeter){
        return new static($centimeter);
    }

    /**
     * @param $meter
     * @return $this
     */
    public static function createByMeter($meter){
        return new static($meter*100);
    }

    /**
     * @param $meter
     * @return $this
     */
    public static function createByMilliMeter($meter){
        return new static($meter/10);
    }

    /**
     * @return integer
     */
    public function getInCm(){
        return $this->centimeter;
    }


}