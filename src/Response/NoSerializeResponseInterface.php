<?php


namespace GoraSu\Components\YandexDeliveryApi\Response;


/**
 * Интерфейс для ответа который не содержит json строку и его невозможно
 * десериализовать
 * Interface NoSerializeResponseInterface
 * @package GoraSu\Components\YandexDeliveryApi\Response
 */
interface NoSerializeResponseInterface
{


    /**
     * NoSerializeResponseInterface constructor.
     * @param string $response
     */
    function __construct($response);


}