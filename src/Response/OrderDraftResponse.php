<?php


namespace GoraSu\Components\YandexDeliveryApi\Response;


/**
 * Ответ на создание черновика
 * Class OrderDraftResponse
 * @package GoraSu\Components\YandexDeliveryApi\Response
 */
class OrderDraftResponse implements NoSerializeResponseInterface
{

    private $orderId;

    public function __construct($response)
    {
        $this->orderId = (int)$response;
    }

    /**
     * @return integer mixed
     */
    public function getOrderId()
    {
        return $this->orderId;
    }
}