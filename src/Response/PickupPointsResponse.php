<?php

namespace GoraSu\Components\YandexDeliveryApi\Response;
use JMS\Serializer\Annotation AS JMS;

/**
 *  Ответ на запрос пунктов самовывоза
 *  https://yandex.ru/dev/delivery-3/doc/dg/reference/put-pickup-points.html#output
 * @todo  доделать остальные параметры которые возвращает запрос
 */
class PickupPointsResponse
{

    /**
     * @JMS\Type("int")
     */
    private $id;

    /**
     * @JMS\Type("string")
     */
    private $type;

    /**
     * @JMS\Type("string")
     */
    private $name;
    /**
     * @JMS\Type("string")
     */
    private $instruction;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getInstruction()
    {
        return $this->instruction;
    }








}