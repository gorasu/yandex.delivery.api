<?php


namespace GoraSu\Components\YandexDeliveryApi\Response;

use GoraSu\Components\YandexDeliveryApi\Response\Type\DeliveryOption\Cost;
use GoraSu\Components\YandexDeliveryApi\Response\Type\DeliveryOption\Delivery;
use GoraSu\Components\YandexDeliveryApi\Response\Type\DeliveryOption\Service;
use GoraSu\Components\YandexDeliveryApi\Response\Type\DeliveryOption\Shipment;
use JMS\Serializer\Annotation AS JMS;


class DeliveryOption
{
    const COST_CLASS = Cost::class;
    const DELIVERY_CLASS = Delivery::class;
    const CLASS_TO_SHIPMENT = "array<GoraSu\Components\YandexDeliveryApi\Response\Type\DeliveryOption\Shipment>";
    const SERVICE_CLASS = "array<GoraSu\Components\YandexDeliveryApi\Response\Type\DeliveryOption\Service>";

    /**
     * @JMS\Type("int")
     */
    private $tariffId;

    /**
     * @JMS\Type("string")
     */
    private $tariffName;

    /**
     * @JMS\Type(DeliveryOption::COST_CLASS)
     */
    private $cost;

    /**
     * @JMS\Type("array")
     */
    private $tags;


    /**
     * @JMS\Type(DeliveryOption::DELIVERY_CLASS)
     */
    private $delivery;

    /**
     * @var Service[]
     * @JMS\Type(DeliveryOption::SERVICE_CLASS)
     */
    private $services = [];


    /**
     * @JMS\Type(DeliveryOption::CLASS_TO_SHIPMENT)
     */
    private $shipments;

    /**
     * @return mixed
     */
    public function getTariffId()
    {
        return $this->tariffId;
    }

    /**
     * @return mixed
     */
    public function getTariffName()
    {
        return $this->tariffName;
    }

    /**
     * @return Cost
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * @return string[]
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * @return Delivery
     */
    public function getDelivery()
    {
        return $this->delivery;
    }

    /**
     * @return Service[]
     */
    public function getServices()
    {
        return $this->services;
    }

    /**
     * @return Shipment[]
     */
    public function getShipments()
    {
        return $this->shipments;
    }







/*
{
"tariffId": 100660,
"tariffName": "СДЭК ПВЗ",
"cost": {
"delivery": 166.00,
"deliveryForSender": 193.738,
"deliveryForCustomer": 193.738
},
"delivery": {
    "partner": {
        "id": 51,
                "code": "СДЭК",
                "name": "СДЭК",
                "partnerType": "DELIVERY",
                "logoUrl": "http://avatars.mds.yandex.net/get-lms/2006792/CDEK_1581004726.svg/orig"
            },
            "type": "PICKUP",
            "calculatedDeliveryDateMin": "2021-01-22",
            "calculatedDeliveryDateMax": "2021-01-22",
            "courierSchedule": null
        },
        "pickupPointIds": [
    10001630386,
    10000007132,
    10001501333,
    10001665370
],
        "shipments": [
            {
                "date": "2021-01-18",
                "type": "WITHDRAW",
                "partner": {
                "id": 133,
                    "code": "Strizh",
                    "name": "МОС Огородный (СЦ)",
                    "partnerType": "SORTING_CENTER",
                    "logoUrl": "http://avatars.mds.yandex.net/get-lms/2006792/SortingCenter2.svg/orig"
                },
                "warehouse": null,
                "settingsDefault": true,
                "alreadyExists": null
            }
        ],
        "services": [
            {
                "name": "Доставка",
                "code": "DELIVERY",
                "cost": 166.00,
                "customerPay": true,
                "enabledByDefault": true
            },
            {
                "name": "Возврат",
                "code": "RETURN",
                "cost": 124.5000,
                "customerPay": false,
                "enabledByDefault": false
            },
            {
                "name": "Сортировка возврата",
                "code": "RETURN_SORT",
                "cost": 20,
                "customerPay": false,
                "enabledByDefault": false
            },
            {
                "name": "Сортировка на едином складе",
                "code": "SORT",
                "cost": 27,
                "customerPay": true,
                "enabledByDefault": true
            },
            {
                "name": "Услуга Объявленная ценность",
                "code": "INSURANCE",
                "cost": 0.738,
                "customerPay": true,
                "enabledByDefault": true
            }
        ],
        "tags": [
    "CHEAPEST"
]
    }*/

}