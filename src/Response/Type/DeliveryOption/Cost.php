<?php


namespace GoraSu\Components\YandexDeliveryApi\Response\Type\DeliveryOption;

use JMS\Serializer\Annotation AS JMS;

/**
 * Class Cost
 * @package GoraSu\Components\YandexDeliveryApi\Response\DeliveryOption
 */
class Cost
{

    /**
     * @JMS\Type("float")
     */
    private $delivery;

    /**
     * @JMS\Type("float")
     */
    private $deliveryForSender;

    /**
     * @JMS\Type("float")
     */
    private $deliveryForCustomer;

    /**
     * @return mixed
     */
    public function getDelivery()
    {
        return $this->delivery;
    }

    /**
     * @return mixed
     */
    public function getDeliveryForSender()
    {
        return $this->deliveryForSender;
    }

    /**
     * @return mixed
     */
    public function getDeliveryForCustomer()
    {
        return $this->deliveryForCustomer;
    }



}