<?php


namespace GoraSu\Components\YandexDeliveryApi\Response\Type\DeliveryOption;

use JMS\Serializer\Annotation AS JMS;

class Shipment
{
    /**
     * @JMS\Type("DateTime<'Y-m-d'>")
     */
    private $date;
    /**
     * @JMS\Type("string")
     */
    private $type;
    /**
     * @JMS\Type("int")
     */
    private $partnerId;
    /**
     * @JMS\Type("int")
     */
    private $warehouseId;
    /**
     * @JMS\Type("boolean")
     */
    private $includeNonDefault;

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return integer
     */
    public function getPartnerId()
    {
        return $this->partnerId;
    }

    /**
     * @return integer
     */
    public function getWarehouseId()
    {
        return $this->warehouseId;
    }

    /**
     * @return boolean
     */
    public function getIncludeNonDefault()
    {
        return $this->includeNonDefault;
    }



}