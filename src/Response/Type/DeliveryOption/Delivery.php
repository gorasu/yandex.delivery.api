<?php


namespace GoraSu\Components\YandexDeliveryApi\Response\Type\DeliveryOption;

use DateTime;
use JMS\Serializer\Annotation AS JMS;


/**
 * Class Delivery
 * @package GoraSu\Components\YandexDeliveryApi\Response\Type\DeliveryOption
 */
class Delivery
{
    /**
     * @JMS\Type("string")
     */
    private $type;

    /**
     * @JMS\Type("DateTime<'Y-m-d'>")
     */
    private $calculatedDeliveryDateMin;

    /**
     * @JMS\Type("DateTime<'Y-m-d'>")
     */
    private $calculatedDeliveryDateMax;

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return DateTime
     */
    public function getCalculatedDeliveryDateMin()
    {
        return $this->calculatedDeliveryDateMin;
    }

    /**
     * @return DateTime
     */
    public function getCalculatedDeliveryDateMax()
    {
        return $this->calculatedDeliveryDateMax;
    }







}