<?php


namespace GoraSu\Components\YandexDeliveryApi\Response\Type\DeliveryOption;

use JMS\Serializer\Annotation AS JMS;

/**
 * Сервисы доставки из которых расчитывается стоимость доставки для магазина
 * Class Service
 * @package GoraSu\Components\YandexDeliveryApi\Response\Type\DeliveryOption
 */
class Service
{


    /**
     * @JMS\Type("string")
     * @var string
     */
    private $name;
    /**
     * @JMS\Type("string")
     * @var string
     */
    private $code;
    /**
     * @JMS\Type("float")
     * @var float
     */
    private $cost;
    /**
     * @JMS\Type("bool")
     * @var bool
     */
    private $customerPay;
    /**
     * @JMS\Type("bool")
     * @var bool
     */
    private $enabledByDefault;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @return float
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * @return bool
     */
    public function isCustomerPay()
    {
        return $this->customerPay;
    }

    /**
     * @return bool
     */
    public function isEnabledByDefault()
    {
        return $this->enabledByDefault;
    }




}