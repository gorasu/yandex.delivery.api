<?php


namespace GoraSu\Components\YandexDeliveryApi;


use GoraSu\Components\YandexDeliveryApi\Request\RequestInterface;
use GoraSu\Components\YandexDeliveryApi\Response\NoSerializeResponseInterface;
use GuzzleHttp\RequestOptions;
use JMS\Serializer\SerializerInterface;

class Client
{

    /**
     * @var string
     */
    private $url = "https://api.delivery.yandex.ru/";

    /**
     * @var \GuzzleHttp\ClientInterface
     */
    private $client;
    /**
     * @var SerializerInterface
     */
    private $serializer;


    /**
     * Client constructor.
     * @param $apiKey
     * @param GuzzleBuilder $builder
     * @param SerializerInterface $serializer
     */
    function __construct($apiKey, GuzzleBuilder $builder, SerializerInterface $serializer)
    {
        $this->client = $builder->build($this->url, $apiKey);
        $this->serializer = $serializer;
    }


    /**
     * Метод который строит запрос к API на основе переданного RequestInterface
     * И возвращает результат запроса сконвертированный в объект
     * @param RequestInterface $request
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @todo отработка ошибок отлов кодов и ответов ошибок ClientException
     */
    function request(RequestInterface $request)
    {

        $params = is_object($request->getRequestParams()) ? json_decode($this->serializer->serialize($request->getRequestParams(), "json")) : $request->getRequestParams();
        $response = $this->client->request($request->getHttpMethod(), $request->getApiMethod(), [
            $this->getRequestOptions($request->getHttpMethod()) =>
                $params

            ,
        ]);
        $json = (string)$response->getBody();
        $response->getBody()->close();

        $class = $request->getResponseClassName();
        if($class && class_exists($class) && in_array(NoSerializeResponseInterface::class, class_implements($class))) {
            return  new $class($json);
        }
        else if ($class) {
            return $this->serializer->deserialize($json, $class, "json");
        }
        if (!$json) {
            return null;
        }

        return json_decode($json, true);
    }


    private function getRequestOptions($method)
    {

        if ($method == RequestInterface::GET) {
            return RequestOptions::QUERY;
        }

        return RequestOptions::JSON;


    }


}